# snapshot.debian.org - web frontend
# https://salsa.debian.org/snapshot-team/snapshot
#
# Copyright (c) 2020 Baptiste Beauplat <lyknode@cilg.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from html import unescape
from pytest import mark


@mark.parametrize('archive,status', (
    (
        None,
        302,
    ),
    (
        'does-not-exists',
        404,
    ),
    (
        'debian',
        200,
    ),
))
def test_archive_dates(client, archive, status, breadcrumbs, archive_view):
    if archive:
        response = client.get(f'/archive/{archive}/')
    else:
        response = client.get('/archive/')

    crumbs = [
        {
            'url': '/',
            'name': 'localhost',
            'sep': '|',
        },
        {
            'name': 'archive:',
            'sep': '',
        },
        {
            'name': archive,
            'sep': '',
        },
    ]

    assert response.status_code == status

    if status == 302:
        assert response.location == 'http://localhost/'
    elif status == 404:
        assert f'Archive "{archive}" does not exist' in \
            unescape(response.data.decode())
    else:
        assert breadcrumbs.exists(crumbs, response.data.decode())
        assert archive_view.has_dates(archive, response.data.decode())


@mark.parametrize('archive,year,month,status,error', (
    ('debian', '', '', 404, 'Year "" is not valid.'),
    ('debian', '2020', 'abc', 404, 'Month "abc" is not valid.'),
    ('debian', '2020', '13', 404,
     'Found no mirrorruns for archive debian in 2020-13.'),
    ('debian', '2020', '8', 200, None),
    ('does-not-exist', '2020', '13', 404,
     'Archive "does-not-exist" does not exist'),
))
def test_archive_runs(client, archive, year, month, status, error, archive_view,
                      breadcrumbs):
    response = client.get(f'/archive/{archive}/?year={year}&month={month}')

    assert response.status_code == status

    if status == 404:
        assert error in unescape(response.data.decode())
    else:
        crumbs = [
            {
                'url': '/',
                'name': 'localhost',
                'sep': '|',
            },
            {
                'name': 'archive:',
                'sep': '',
            },
            {
                'url': '/archive/debian/',
                'name': 'debian',
                'sep': '',
            },
            {
                'name': f'({year}-{int(month):02})',
            },
        ]

        assert breadcrumbs.exists(crumbs, response.data.decode())
        assert archive_view.has_runs('debian', year, month,
                                     response.data.decode())


@mark.parametrize('date,status,error,redirect,path', (
    ('99999999T999999Z', 404,
        'Invalid date string - nothing to be found here.', None, ''),
    ('wrong-date', 404,
        'Invalid date string - nothing to be found here.', None, ''),
    ('19700101T000000Z', 404, 'No mirrorrun found at this date.', None, ''),
    ('20200813T185438Z', 200, None, None, '/'),
    ('20200813T185438Z', 200, None, None, '/dists'),
    ('20200813T185438Z', 404, 'No such file or directory', None,
        '/été'),
    ('20200813T185438Z', 404, 'No such file or directory', None,
        '/dists///////not-found'),
    ('20200813T185438Z', 302, None, '20200813T185438Z/dists/unstable',
        '/dists/sid'),
    ('40000813T185438Z', 302, None, '20200813T185438Z', ''),
    ('now', 302, None, '20200813T185438Z', ''),
))
def test_archive_files(client, date, status, error, redirect, path,
                       archive_view, breadcrumbs):
    response = client.get(f'/archive/debian/{date}{path}/'.replace('//', '/'))

    assert response.status_code == status

    if status == 404:
        assert error in unescape(response.data.decode())
    elif status == 302:
        assert response.location == \
            f'http://localhost/archive/debian/{redirect}/'
    else:
        crumbs = [
            {
                'url': '/',
                'name': 'localhost',
                'sep': '|',
            },
            {
                'name': 'archive:',
                'sep': '',
            },
            {
                'url': '/archive/debian/',
                'name': 'debian',
                'sep': '',
            },
            {
                'url': f'/archive/debian/?year={date[0:4]}&'
                       f'month={int(date[4:6])}',
                'name': f'({date[0:4]}-{date[4:6]})',
            },
        ]

        components = list(filter(bool, path.split('/')))

        if components:
            crumbs.append({
                'url': f'/archive/debian/{date}/',
                'name': f'{date[0:4]}-{date[4:6]}-{date[6:8]} {date[9:11]}:'
                        f'{date[11:13]}:{date[13:15]}',
            })

            url = ''

            for component in components:
                url += f'{component}/'

                crumbs.append({
                    'url': f'/archive/debian/{date}/{url}',
                    'name': component,
                })

            crumbs[-1].pop('url')

        else:
            crumbs.append({
                'name': f'{date[0:4]}-{date[4:6]}-{date[6:8]} {date[9:11]}:'
                        f'{date[11:13]}:{date[13:15]}',
            })

        assert breadcrumbs.exists(crumbs, response.data.decode())
        assert archive_view.has_timeline('debian', date, path,
                                         response.data.decode())
        assert archive_view.has_files('debian', date, path,
                                      response.data.decode())


@mark.parametrize('date,status,error,content,path', (
    ('20200813T185438Z', 200, None, 'Suite: unstable',
        '/dists/unstable/Release'),
    ('20200813T185438Z', 404, 'No such file or directory', None, '/not-found'),
))
def test_archive_content(client, date, status, error, content, path):
    response = client.get(f'/archive/debian/{date}{path}/')

    assert response.status_code == status

    if status == 404:
        assert error in unescape(response.data.decode())
    else:
        assert content in unescape(response.data.decode())


@mark.parametrize('date,content,path', (
    ('20200813T185438Z', 'Suite: unstable',
        '/dists/unstable/Release'),
))
def test_archive_redirect_farm(client, config, date, content, path):
    config['REDIRECT_TO_FARM'] = True

    response = client.get(f'/archive/debian/{date}{path}/')

    assert response.status_code == 302
    assert '/file/' in response.location

    response = client.get(response.location)

    assert response.status_code == 200
    assert content in unescape(response.data.decode())
