# snapshot.debian.org - web frontend
# https://salsa.debian.org/snapshot-team/snapshot
#
# Copyright (c) 2020 Baptiste Beauplat <lyknode@cilg.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

REMOVAL_ENTIRES = (
    {
        'id': 1,
        'timestamp': '2020-09-11 21:05:51.572359',
        'files': (
            'tmux_1.3_amd64.deb',
            'tmux_1.3.dsc',
            'tmux_1.3.tar.xz',
        ),
        'log': """hello - unredistributable

Package is missing sources (closes: #42).

 -- Vincent Time (vtime) on host localhost at Sat, 11 Sep 2020 21:05:51 +0200
""",
    },
    {
        'id': 2,
        'timestamp': '2020-09-12 21:05:51.572359',
        'files': (
            'zsh_1.10_amd64.deb',
            'zsh_1.10.dsc',
            'zsh_1.10.tar.xz',
        ),
        'log': """goodbye - unredistributable

Package is missing sources (closes: #1).

 -- Vincent Time (vtime) on host localhost at Sat, 12 Sep 2020 21:05:51 +0200
""",
    },
)


def test_removal(client, removal):
    for entry in REMOVAL_ENTIRES:
        removal.add(**entry)

    response = client.get('/removal/')

    assert response.status_code == 200
    for entry in REMOVAL_ENTIRES:
        assert entry['log'].splitlines()[0] in response.data.decode()
        assert entry['timestamp'] in response.data.decode()

    for bug in ('42', '1',):
        assert f'<a href="https://bugs.debian.org/{bug}">#{bug}</a>' in \
            response.data.decode()

    for index in [entry['id'] for entry in REMOVAL_ENTIRES]:
        assert f'<a href="{index}">[affected files]</a>' in \
            response.data.decode()


def test_removal_one(client, removal):
    for entry in REMOVAL_ENTIRES:
        removal.add(**entry)

    response = client.get('/removal/3')

    assert response.status_code == 404
    assert 'Removal log not found' in \
        response.data.decode()

    response = client.get('/removal/1')

    assert response.status_code == 200

    entry = REMOVAL_ENTIRES[0]
    assert entry['log'].splitlines()[0] in response.data.decode()
    assert entry['timestamp'] in response.data.decode()
    assert '<a href="https://bugs.debian.org/42">#42</a>' in \
        response.data.decode()

    for filename in entry['files']:
        assert removal.exists_in_affected_files(filename,
                                                response.data.decode())
